TARGET = programm
CC ?= gcc
FLAGS += -g -Wall -Wextra -Wpedantic -std=c11
SRC = $(wildcard *.c)

all: $(TARGET)

$(TARGET) : $(SRC)
	$(CC) $(FLAGS) $(SRC) -o $(TARGET)

clean:
	rm $(TARGET)
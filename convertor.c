#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#include "convertor.h"

/* Определение номера символа, с которого начинаются различия с ASCII */
uint8_t begin_diff_ch(char *code)
{
    if (strcmp(code, KOI8) == 0)
    {
        return BEGIN_DIFF_KOI8;
    }
    else if (strcmp(code, CP1251) == 0)
    {
        return BEGIN_DIFF_CP1251;
    }
    else if (strcmp(code, ISO8859) == 0)
    {
        return BEGIN_DIFF_ISO8859;
    }
    return 0;
}

/* Считывание необходимых данных для конвертации */
uint16_t *read_decode(char *name_decode)
{
    FILE *decode;
    uint16_t *conv_sym;
    struct stat info_encoding;

    if((decode = fopen(name_decode, "rb")) == NULL)
    {
        return NULL;
    }

    stat ((const char *)name_decode, &info_encoding);

    conv_sym = (uint16_t *) malloc(info_encoding.st_size);
    fread(conv_sym, 1, info_encoding.st_size, decode);

    fclose(decode);
    return conv_sym;
}

/* конвертация и запись в файл */
char *wr_decode(char *code_name, char *wr_decode_name, uint16_t *decode, uint8_t begin_diff)
{
    FILE *code;
    FILE *wr_file;
    struct stat info_file;
    uint8_t *old_sym;

    if((code = fopen(code_name, "rb")) == NULL)
    {
        return code_name;
    }

    if((wr_file = fopen(wr_decode_name, "wb")) == NULL)
    {
        return wr_decode_name;
    }

    stat ((const char *)code_name, &info_file);
    old_sym = (uint8_t *) malloc(1);
    for (int i = 0; i < info_file.st_size; i++)
    {
        fread(old_sym, 1, 1, code);
        if (*old_sym < 128)
        {
            fwrite(old_sym, sizeof(unsigned char), 1, wr_file);
        }
        else if (*old_sym > begin_diff - 1)
        {
            uint16_t *decode_p = (decode + (*old_sym - begin_diff));
            if ((uint8_t) *decode_p < MIN_HIGH_BYTE)
            {
                uint8_t add_byte = ADDING_BYTE;
                fwrite(&add_byte, sizeof(unsigned char), 1, wr_file);
            }
            fwrite(decode_p, sizeof(unsigned char), 2, wr_file);
        }
    }
    free(old_sym);
    fclose(code);
    fclose(wr_file);
    return NULL;
}

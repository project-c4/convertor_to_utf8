#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "convertor.h"

void print_help()
{
    printf("./programm [Read file] [Encoding] [Write file]\n\n");

    printf("Program for converting from one encoding to UTF8\n\n");

    printf("Supported encodings:\n");
    printf("cp1251\n");
    printf("iso-8859-5\n");
    printf("koi8\n");
}

int main(int argc, char **argv)
{
    uint16_t *conv_sym;     /* Данные для преобразования символов для выбранной кодировки */
    uint8_t begin_diff;     /* Номер символа, с которого начинаются различия с ASCII и выбранной кодировки */
    char *error_file;       /* Имя файла, в котором произошла ошибка при конвертации */
    errno = 0;

    for (int i = 0; i < argc; i++)
    {
        if(strcmp(argv[i], "-h") == 0)
        {
            print_help();
            return 0;
        }
    }

    if (argc != 4)
    {
        print_help();
        return 0;
    }

    begin_diff = begin_diff_ch(argv[2]);

    if ((conv_sym = read_decode(argv[2])) == NULL)
    {
        if (errno == ENOENT)
        {
            fprintf(stderr, "Error message : %s : No encoding information\n", argv[2]);
        }
        else
        {
            char *errorbuf = strerror(errno);
            fprintf(stderr, "Error message : %s : %s\n", argv[2], errorbuf);
        }
        return errno;
    }
    
    if ((error_file = wr_decode(argv[1], argv[3], conv_sym, begin_diff)) != NULL)
    {
        char *errorbuf = strerror(errno);
        fprintf(stderr, "Error message : %s : %s\n", error_file, errorbuf);
        return errno;
    }

    free(conv_sym);
    return 0;
}
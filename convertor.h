#include <stdio.h>
#include <stdint.h>
#include <errno.h>

#ifndef CONVERTOR_H
#define CONVERTOR_H

/* Названия поддерживаемых кодировок */
#define KOI8        "koi8"
#define CP1251      "cp1251"
#define ISO8859     "iso-8859-5"

/* Номера символов, с которых начинаются расхождения с UTF8 в различных кодировках*/
#define BEGIN_DIFF_KOI8     192
#define BEGIN_DIFF_CP1251   128
#define BEGIN_DIFF_ISO8859  160

#define MIN_HIGH_BYTE   0xD0    /* Минимальное значение старшего байта в 2-байтных символах UTF8 */
#define ADDING_BYTE     0xE2    /* Значение 3-байта для 3-х байтных символов */

/* Определение номера символа, с которого начинаются различия с UTF8 */
uint8_t begin_diff_ch(char* code);

/* Считывание необходимых данных для конвертации */
uint16_t *read_decode(char *name_decode);

/* конвертация и запись в файл */
char *wr_decode(char *code_name, char *wr_decode_name, uint16_t *decode, uint8_t begin_diff);

#endif /* CONVERTOR_H */